The program trains a Naive Bayes classifier and uses this model classify restaurant reviews as being either positive or negative.
Detailed description of files and their purpose can be found in assignment_description.pdf
