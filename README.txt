README
Author: Kaile Baran (kbaran)
Assignment: 3

For restaurant-competition-P1.py I did not use argparse, rather, I used sys.argv[] to take inputs from the command line. Please just enter restaurant-competition-P1.py filenametoread.[txt|tsv] filenametowrite.txt
	There are occasionally problems with input where specifying data/filename.tsv in the command line will result in the program looking in data/data/filename.tsv so please be aware of that.

All required files are present, however, I was unsure of what exactly I should have named the prediction files so they are all named feature_type_dev_predictions.txt

There are 5 models at the bottom that I included, these have been trained and I figured I may as well include them.