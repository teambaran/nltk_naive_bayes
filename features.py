
import nltk
import re
import word_category_counter
import data_helper
import os, sys
from nltk import *

DATA_DIR = "data"
LIWC_DIR = "liwc"

word_category_counter.load_dictionary(LIWC_DIR)


def normalize(token, should_normalize=True):
    """
    This function performs text normalization.

    If should_normalize is False then we return the original token unchanged.
    Otherwise, we return a normalized version of the token, or None.

    For some tokens (like stopwords) we might not want to keep the token. In
    this case we return None.

    :param token: str: the word to normalize
    :param should_normalize: bool
    :return: None or str
    """
    normalized_token = ''
    if not should_normalize:
        normalized_token = token

    else:
        stop_words = nltk.corpus.stopwords.words('english')
        p = re.compile(r'([^\w\s]*[a-z|A-Z|0-9]+)')
        token = token.lower()
        if p.match(token) and token not in stop_words:
            return token
        else:
            return None

    return normalized_token



def get_words_tags(text, should_normalize=True):
    """
    This function performs part of speech tagging and extracts the words
    from the review text.

    You need to :
        - tokenize the text into sentences
        - word tokenize each sentence
        - part of speech tag the words of each sentence

    Return a list containing all the words of the review and another list
    containing all the part-of-speech tags for those words.

    :param text:
    :param should_normalize:
    :return:
    """
    words = []
    tags = []
    text = sent_tokenize(text)
    
    for idx,sentence in enumerate(text):
        text[idx] = pos_tag(word_tokenize(sentence))
    
    if should_normalize:
        for idx,sentence in enumerate(text):
            for idy,word in enumerate(sentence):
                #if (idy == 0 or idy == 1 or idy == 2) and idx == 0: #removing unneeded info at beginning of sentence, only applies to first sentence.
                   # pass
                #else:
                temp = normalize(word[0])
                if temp is not None:
                    words.append(temp)
                    tags.append(word[1])
                else:
                    pass
        #words.insert(0,text[0][1][0])
        #tags.insert(0, text[0][1][0])
    else:  
        for idx,sentence in enumerate(text):
            for idy,word in enumerate(sentence):
                temp = normalize(word[0])
                if temp is not None:
                    words.append(temp)
                    tags.append(word[1])
                else:
                    pass    

    return words, tags


def get_ngram_features(tokens):
    """
    This function creates the unigram and bigram features as described in
    the assignment3 handout.

    :param tokens:
    :return: feature_vectors: a dictionary values for each ngram feature
    """
    feature_vectors = {}
    review_type = ''
    if tokens[0] == 'pos' or tokens[0] == 'neg':
        review_type = tokens[0]
        tokens = tokens[1:]
    most_common_uni = FreqDist(tokens).most_common()
    most_common_bi = ConditionalFreqDist(bigrams(tokens))
    bigram_counts = []
    for item in most_common_bi.items():
        the_words = item[1].most_common()
        cond_word = item[0]
        for pair in the_words:
            bigram_counts.append((cond_word,pair[0],pair[1]))
    for uni in most_common_uni:
        key = "UNI_" + uni[0]
        value = bin(uni[1])
        feature_vectors[key] = value
    for bi in bigram_counts:
        key = "BIGRAM_" + bi[0] + '_' + bi[1]
        value = bin(bi[2])
        feature_vectors[key] = value
    if review_type:
        feature_vectors["REVIEW_TYPE"] = review_type
    return feature_vectors


def get_pos_features(tags):
    """
    This function creates the unigram and bigram part-of-speech features
    as described in the assignment3 handout.

    :param tags: list of POS tags
    :return: feature_vectors: a dictionary values for each ngram-pos feature
    """
    feature_vectors = {}
    
    review_type = ''
    
    if tags[0] == 'pos' or tags[0] == 'neg':
        review_type = tags[0]
        tags = tags[1:]
    uni_pos = FreqDist(tags).most_common()
    bi_pos = ConditionalFreqDist(bigrams(tags))
    bi_counts = []
    
    for item in bi_pos.items():
        the_words = item[1].most_common()
        cond_word = item[0]
        for pair in the_words:
            bi_counts.append((cond_word,pair[0],pair[1]))
    for pos in uni_pos:
        key = "UNI_POS_" + pos[0]
        value = bin(pos[1])
        feature_vectors[key] = value
    for bi in bi_counts:
        key = "BI_POS_" + bi[0] + "_" + bi[1]
        value = bin(bi[2])
        feature_vectors[key] = value
        
    """if review_type:
        feature_vectors["REVIEW_TYPE"] = review_type"""
    return feature_vectors


def bin(count):
    """
    Results in bins of  0, 1, 2, 3 >=
    :param count: [int] the bin label
    :return:
    """
    #the_bin = None
    return count if count < 2 else 3

    #return the_bin


def get_liwc_features(words):
    """
    Adds a simple LIWC derived feature

    :param words:
    :return:
    """

    # TODO: binning

    feature_vectors = {}
    text = " ".join(words)
    liwc_scores = word_category_counter.score_text(text)

    # All possible keys to the scores start on line 269
    # of the word_category_counter.py script

    liwc_features = [category[0] for category in word_category_counter.Dictionary._liwc_categories]
    if words[0] == 'pos' or words[0] == 'neg':
        review_type = words[0]
        words = words[1:]
    for feature in liwc_features:
        key = "LIWC:" + feature
        feature_vectors[key] = bin(liwc_scores[feature])
    """negative_score = liwc_scores["Negative Emotion"]
    positive_score = liwc_scores["Positive Emotion"]
    
    feature_vectors["Negative Emotion"] = negative_score
    feature_vectors["Positive Emotion"] = positive_score

    if positive_score > negative_score:
        feature_vectors["liwc:positive"] = 1
    else:
        feature_vectors["liwc:negative"] = 1"""

    """if review_type:
        feature_vectors["REVIEW_TYPE"] = review_type"""
    return feature_vectors


FEATURE_SETS = {"word_pos_features", "word_features", "word_pos_liwc_features"}

def get_features_category_tuples(category_text_dict, feature_set):
    """

    You will might want to update the code here for the competition part.

    :param category_text_dict:
    :param feature_set:
    :return:
    """
    features_category_tuples = []
    all_texts = []

    assert feature_set in FEATURE_SETS, "unrecognized feature set:{}, Accepted values:{}".format(feature_set, FEATURE_SETS)

    for category in category_text_dict:
        for text in category_text_dict[category]:

            words, tags = get_words_tags(text)
            feature_vectors = {}
            if feature_set == "word_features":
                feature_vectors = get_ngram_features(words)
            elif feature_set == "word_pos_features":
                feature_vectors = get_ngram_features(words)
                temp = get_pos_features(tags)
                feature_vectors.update(temp)
            elif feature_set == "word_pos_liwc_features":
                feature_vectors = get_ngram_features(words)
                feature_vectors.update(get_pos_features(tags))
                feature_vectors.update(get_liwc_features(words))

            features_category_tuples.append((feature_vectors, category))
            all_texts.append(text)

    return features_category_tuples, all_texts #all_texts should just be a list of reviews (I think?)


def write_features_category(features_category_tuples, outfile_name):
    """
    Save the feature values to file.

    :param features_category_tuples:
    :param outfile_name:
    :return:
    """
    with open(outfile_name, "w", encoding="utf-8") as fout:
        for (features, category) in features_category_tuples:
            fout.write("{0:<10s}\t{1}\n".format(category, features))






if __name__ == "__main__":
    #DEVELOPMENT
    content = []
    with open("data/dev_examples.tsv",  "r", encoding="utf-8") as fin:
        content = fin.readlines()
    words = []
    pos = []
    for review in content:
        words.append(get_words_tags(review)[0])
        pos.append(get_words_tags(review)[1])
    
    sys.stdout = open('WORD-FEATURES-DEVELOPMENT-features.txt','w',encoding="utf-8")
    for idx,review in enumerate(words):
        if idx == 0:
            pass
        else:
            features = get_ngram_features(review)
            review_type = features.pop("REVIEW_TYPE", None)
            review_type = 'positive' if (review_type == 'pos') else 'negative'
            print(review_type,"\t", end='')
            print(features)
    sys.stdout = open('WORD-POS-FEATURES-DEVELOPMENT-features.txt','w',encoding="utf-8")
    for idx,review in enumerate(words):
        if idx == 0:
            pass
        else:
            ngram_features = get_ngram_features(review)
            pos_features = get_pos_features(pos[idx])
            review_type = ngram_features.pop("REVIEW_TYPE", None)
            review_type = 'positive' if (review_type == 'pos') else 'negative'
            ngram_features.update(pos_features)
            print(review_type,"\t",end='')
            print(ngram_features)

    sys.stdout = open('WORD-POS-LIWC-FEATURES-DEVELOPMENT-features.txt','w',encoding="utf-8")
    for idx,review in enumerate(words):
        if idx == 0:
            pass
        else:
            ngram_features = get_ngram_features(review)
            pos_features = get_pos_features(pos[idx])
            liwc_features = get_liwc_features(review)
            review_type = ngram_features.pop("REVIEW_TYPE", None)
            review_type = 'positive' if (review_type == 'pos') else 'negative'
            ngram_features.update(pos_features)
            ngram_features.update(liwc_features)
            print(review_type,"\t",end='')
            print(ngram_features)
    #TESTING
    content = []
    with open("data/train_examples.tsv",  "r", encoding="utf-8") as fin:
        content = fin.readlines()
    words = []
    pos = []
    for review in content:
        words.append(get_words_tags(review)[0])
        pos.append(get_words_tags(review)[1])
    
    sys.stdout = open('WORD-FEATURES-TRAINING-features.txt','w',encoding="utf-8")
    for idx,review in enumerate(words):
        if idx == 0:
            pass
        else:
            features = get_ngram_features(review)
            review_type = features.pop("REVIEW_TYPE", None)
            review_type = 'positive' if (review_type == 'pos') else 'negative'
            print(review_type,"\t", end='')
            print(features)
        
    sys.stdout = open('WORD-POS-FEATURES-TRAINING-features.txt','w',encoding="utf-8")
    for idx,review in enumerate(words):
        if idx == 0:
            pass
        else:
            ngram_features = get_ngram_features(review)
            pos_features = get_pos_features(pos[idx])
            review_type = ngram_features.pop("REVIEW_TYPE", None)
            review_type = 'positive' if (review_type == 'pos') else 'negative'
            ngram_features.update(pos_features)
            print(review_type,"\t",end='')
            print(ngram_features)
    
    sys.stdout = open('WORD-POS-LIWC-FEATURES-TRAINING-features.txt','w',encoding="utf-8")
    for idx,review in enumerate(words):
        if idx == 0:
            pass
        else:
            ngram_features = get_ngram_features(review)
            pos_features = get_pos_features(pos[idx])
            liwc_features = get_liwc_features(review)
            review_type = ngram_features.pop("REVIEW_TYPE", None)
            review_type = 'positive' if (review_type == 'pos') else 'negative'
            ngram_features.update(pos_features)
            ngram_features.update(liwc_features)
            print(review_type,"\t",end='')
            print(ngram_features)
    
    #TESTING
    content = []
    with open("data/test.txt",  "r", encoding="utf-8") as fin:
        content = fin.readlines()
    words = []
    pos = []
    for review in content:
        words.append(get_words_tags(review, False)[0])
        pos.append(get_words_tags(review, False)[1])
    
    sys.stdout = open('WORD-FEATURES-TESTING-features.txt','w',encoding="utf-8")
    for idx,review in enumerate(words):
        features = get_ngram_features(review)
        print(features)
        
    sys.stdout = open('WORD-POS-FEATURES-TESTING-features.txt','w',encoding="utf-8")
    for idx,review in enumerate(words):

        ngram_features = get_ngram_features(review)
        pos_features = get_pos_features(pos[idx])
        ngram_features.update(pos_features)
        print(ngram_features)
    
    sys.stdout = open('WORD-POS-LIWC-FEATURES-TESTING-features.txt','w',encoding="utf-8")
    for idx,review in enumerate(words):
        ngram_features = get_ngram_features(review)
        pos_features = get_pos_features(pos[idx])
        liwc_features = get_liwc_features(review)
        ngram_features.update(pos_features)
        ngram_features.update(liwc_features)
        print(ngram_features)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

