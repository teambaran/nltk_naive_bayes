
import re, nltk, pickle, argparse
import os
import data_helper
import ast
import sys
from features import *

DATA_DIR = "data"
global outfile

def write_features_category(features_category_tuples, output_file_name):
    output_file = open("{}-features.txt".format(output_file_name), "w", encoding="utf-8")
    for (features, category) in features_category_tuples:
        output_file.write("{0:<10s}\t{1}\n".format(category, features))
    output_file.close()


def get_classifier(classifier_fname):
    classifier_file = open(classifier_fname, 'rb')
    classifier = pickle.load(classifier_file)
    classifier_file.close()
    return classifier


def save_classifier(classifier, classifier_fname):
    classifier_file = open(classifier_fname, 'wb')
    pickle.dump(classifier, classifier_file)
    classifier_file.close()
    info_file = open(classifier_fname.split(".")[0] + '-informative-features.txt', 'w', encoding="utf-8")
    for feature, n in classifier.most_informative_features(100):
        info_file.write("{0}\n".format(feature))
    info_file.close()


def evaluate(classifier, features_category_tuples, reference_text, data_set_name=None):

    ###     YOUR CODE GOES HERE
    if isinstance(features_category_tuples[0], tuple):
        accuracy = nltk.classify.accuracy(classifier, features_category_tuples)
    
    #print("{0:6s} {1:8.5f}".format("Dev", accuracy))
    
        features_only = [example[0] for example in features_category_tuples]
    
        reference_labels = [example[1] for example in features_category_tuples]
        predicted_labels = classifier.classify_many(features_only)
    
        confusion_matrix = nltk.ConfusionMatrix(reference_labels, predicted_labels)
    #print(confusion_matrix)
    
    predictions = []
    for feature in features_category_tuples:
        if isinstance(feature, tuple):
            predictions.append(classifier.classify(feature[0]))
        else:
            predictions.append(classifier.classify(feature))
    fout = open(outfile, "w+", encoding="utf-8")
    for prediction in predictions:
        fout.write(prediction + "\n")
    
    
    
    
    return accuracy, confusion_matrix


def build_features(data_file, feat_name, save_feats=None):# binning=False):
    # read text data

    positive_texts, negative_texts = data_helper.get_reviews(os.path.join(DATA_DIR, data_file))

    category_texts = {"positive": positive_texts, "negative": negative_texts}

        # build features
    features_category_tuples, texts = get_features_category_tuples(category_texts, feat_name)

        # save features to file
    if save_feats is not None:
        write_features_category(features_category_tuples, save_feats)

    return features_category_tuples, texts



def train_model(datafile, feature_set, save_model=None):

    features_data, texts = build_features(datafile, feature_set)

    ###     YOUR CODE GOES HERE
    classifier = nltk.classify.NaiveBayesClassifier.train(features_data)

    if save_model is not None:
        save_classifier(classifier, save_model)
    return classifier


def train_eval(train_file, feature_set, eval_file=None):

    # train the model
    split_name = "train"
    model = train_model(train_file, feature_set, feature_set + "-model")#, binning=binning)
    
    temp_filename = train_file
    cut = '.'
    temp_filename = temp_filename.split(cut,1)[0]
    
    sys.stdout = open(feature_set + "-" + temp_filename + '-informative-features.txt','w',encoding="utf-8")
    model.show_most_informative_features(20)
    sys.stdout = sys.__stdout__
    

    # save the model
    if model is None:
        model = get_classifier(classifier_fname)

    # evaluate the model
    if eval_file is not None:
        features_data, texts = build_features(eval_file, feature_set)#, binning=binning)
        accuracy, cm = evaluate(model, features_data, texts, data_set_name=None)
        f=open("all-results.txt", "a+")
        f.write(feature_set+"\n")
        f.write("The accuracy of {} is: {}".format(eval_file, accuracy) + "\n")
        f.write("Confusion Matrix:\n")
        f.write(str(cm)+"\n")
        
    else:
        accuracy = None

    return accuracy


def main():

    global outfile

    infile = sys.argv[1]
    outfile = sys.argv[2]
    
    if "test.txt" in infile:
        to_classify = data_helper.get_reviews(infile)
        load_c = get_classifier("word_features-model")
    
        words = []
        pos = []
        feature_vectors = {}

        for review in to_classify:
            w,t = get_words_tags(review)
            words.append(w)
            pos.append(t)
        for idx,review in enumerate(words):
            feature_vectors = {}
            feature_vectors = get_ngram_features(review)
            feature_vectors.update(get_pos_features(pos[idx]))
            feature_vectors.update(get_liwc_features(review))
            words[idx] = feature_vectors
        predictions = []
        for review in words:
            predictions.append(load_c.classify(review))
        fout = open(outfile, "w+", encoding="utf-8")
        for prediction in predictions:
            fout.write(prediction + "\n")

    else:
        train_data = "train_examples.tsv"
        """for feat_set in ["word_features", "word_pos_features", "word_pos_liwc_features"]:
            print("\nTraining with {}".format(feat_set))"""
        acc = train_eval(train_data, "word_features", eval_file=infile)


if __name__ == "__main__":
    main()




